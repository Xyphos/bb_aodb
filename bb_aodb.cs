using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using VhaBot;

namespace bb_aodb
{
    using DIC_T = Dictionary<UInt32, Dictionary<UInt32, KeyValuePair<UInt32, Int64>>>; // <Type, <Instance, <Volume, Position>>>

    public sealed class BbAoDb : PluginBase
    {
        public enum ItemAttributes
        {
            Flags = 0,
            MaxHealth = 1,
            Mass = 2,
            AttackSpeed = 3,
            Breed = 4,
            Organization = 5,
            Team = 6,
            State = 7,
            Duriation = 8,
            MapFlags = 9,
            ProfessionLevel = 10,
            PreviousHealth = 11,
            Mesh = 12,
            Anim = 13,
            Name = 14,
            Info = 15,
            Strength = 16,
            Agility = 17,
            Stamina = 18,
            Intelligence = 19,
            Sense = 20,
            Psychic = 21,
            AMS = 22,
            StaticInstance = 23,
            MaxMass = 24,
            StaticType = 25,
            Energy = 26,
            Health = 27,
            Height = 28,
            DMS = 29,
            Can = 30,
            Face = 31,
            HairMesh = 32,
            Faction = 33,
            DeadTimer = 34,
            AccessCount = 35,
            AttackCount = 36,
            TitleLevel = 37,
            BackMesh = 38,
            ShoulderMesh = 39,
            AlienXP = 40,
            FabricType = 41,
            CATMesh = 42,
            ParentType = 43,
            ParentInstance = 44,
            BeltSlots = 45,
            BandolierSlots = 46,
            Girth = 47,
            ClanLevel = 48,
            InsuranceTime = 49,
            InventoryTimeout = 50,
            AggDef = 51,
            XP = 52,
            IP = 53,
            Level = 54,
            InventoryId = 55,
            TimeSinceCreation = 56,
            LastXP = 57,
            Age = 58,
            Gender = 59,
            Profession = 60,
            Credits = 61,
            Alignment = 62,
            Attitude = 63,
            HeadMesh = 64,
            HairTexture = 65,
            ShoulderTexture = 66,
            HairColourRGB = 67,
            NumConstructedQuest = 68,
            MaxConstructedQuest = 69,
            SpeedPenalty = 70,
            TotalMass = 71,
            ItemType = 72,
            RepairDifficulty = 73,
            Value = 74,
            NanoStrain = 75,
            EquipmentPage = 76,
            RepairSkill = 77,
            CurrentMass = 78,
            Icon = 79,
            PrimaryItemType = 80,
            PrimaryItemInstance = 81,
            SecondaryItemType = 82,
            SecondaryItemInstance = 83,
            UserType = 84,
            UserInstance = 85,
            AreaType = 86,
            AreaInstance = 87,
            DefaultPos = 88,
            Breed2 = 89,
            ProjectileAC = 90,
            MeleeAC = 91,
            EnergyAC = 92,
            ChemicalAC = 93,
            RadiationAC = 94,
            ColdAC = 95,
            PoisonAC = 96,
            FireAC = 97,
            StateAction = 98,
            ItemAnim = 99,
            MartialArts = 100,
            MultiMelee = 101,
            Blunt1h = 102,
            Edged1h = 103,
            MeleeEnergy = 104,
            Edged2h = 105,
            Piercing = 106,
            Blunt2h = 107,
            SharpObjects = 108,
            Grenade = 109,
            HeavyWeapons = 110,
            Bow = 111,
            Pistol = 112,
            Rifle = 113,
            MG_SMG = 114,
            Shotgun = 115,
            AssaultRifle = 116,
            VehicleWater = 117,
            MeleeInit = 118,
            RangedInit = 119,
            PhysicalInit = 120,
            BowSpecialAttack = 121,
            SensoryImprovement = 122,
            FirstAid = 123,
            Treatment = 124,
            MechanicalEngineering = 125,
            ElectricalEngineering = 126,
            MaterialMetamorphose = 127,
            BiologicalMetamorphose = 128,
            PsychologicalModification = 129,
            MaterialCreation = 130,
            SpaceTime = 131,
            NanoPool = 132,
            RangedEnergy = 133,
            MultiRanged = 134,
            TrapDisarm = 135,
            Perception = 136,
            Adventuring = 137,
            Swimming = 138,
            VehicleAir = 139,
            MapNavigation = 140,
            Tutoring = 141,
            Brawl = 142,
            Riposte = 143,
            Dimach = 144,
            Parry = 145,
            SneakAttack = 146,
            FastAttack = 147,
            Burst = 148,
            NanoInit = 149,
            FlingShot = 150,
            AimedShot = 151,
            BodyDevelopment = 152,
            DuckExplosions = 153,
            DodgeRanged = 154,
            EvadeClose = 155,
            RunSpeed = 156,
            QuantumFT = 157,
            WeaponSmithing = 158,
            Pharmaceuticals = 159,
            NanoProgramming = 160,
            ComputerLiteracy = 161,
            Psychology = 162,
            Chemistry = 163,
            Concealment = 164,
            BreakingEntry = 165,
            VehicleGround = 166,
            FullAuto = 167,
            NanoResist = 168,
            AlienLevel = 169,
            HealthChangeBest = 170,
            HealthChangeWorst = 171,
            HealthChange = 172,
            CurrentMovementMode = 173,
            PrevMovementMode = 174,
            AutoLockTimeDefault = 175,
            AutoUnlockTimeDefault = 176,
            MoreFlags = 177,
            AlienNextXP = 178,
            NPCFlags = 179,
            CurrentNCU = 180,
            MaxNCU = 181,
            Specialization = 182,
            EffectIcon = 183,
            BuildingType = 184,
            BuildingInstance = 185,
            CardOwnerType = 186,
            CardOwnerInstance = 187,
            BuildingComplexInst = 188,
            ExitInstance = 189,
            NextDoorInBuilding = 190,
            LastConcretePlayfieldInstance = 191,
            ExtenalPlayfieldInstance = 192,
            ExtenalDoorInstance = 193,
            InPlay = 194,
            AccessKey = 195,
            ConflictReputation = 196,
            OrientationMode = 197,
            SessionTime = 198,
            RP = 199,
            Conformity = 200,
            Aggressiveness = 201,
            Stability = 202,
            Extroverty = 203,
            Taunt = 204,
            ReflectProjectileAC_Percent = 205, //%
            ReflectMeleeAC_Percent = 206, //%
            ReflectEnergyAC_Percent = 207, //%
            ReflectChemicalAC_Percent = 208, //%
            WeaponMesh = 209,
            RechargeDelay = 210,
            EquipDelay = 211,
            MaxEnergy = 212,
            TeamFaction = 213,
            CurrentNano = 214,
            GmLevel = 215,
            ReflectRadiationAC_Percent = 216, //%
            ReflectColdAC_Percent = 217, //%
            ReflectNanoAC_Percent = 218, //%
            ReflectFireAC_Percent = 219, //%
            CurrentBodyLocation = 220,
            MaxNanoEnergy = 221,
            AccumulatedDamage = 222,
            CanChangeClothes = 223,
            Features = 224,
            ReflectPoisonAC_Percent = 225, //%
            ShieldProjectileAC = 226,
            ShieldMeleeAC = 227,
            ShieldEnergyAC = 228,
            ShieldChemicalAC = 229,
            ShieldRadiationAC = 230,
            ShieldColdAC = 231,
            ShieldNanoAC = 232,
            ShieldFireAC = 233,
            ShieldPoisonAC = 234,
            BerserkMode = 235,
            InsurancePercentage = 236,
            ChangeSideCount = 237,
            AbsorbProjectileAC = 238,
            AbsorbMeleeAC = 239,
            AbsorbEnergyAC = 240,
            AbsorbChemicalAC = 241,
            AbsorbRadiationAC = 242,
            AbsorbColdAC = 243,
            AbsorbFireAC = 244,
            AbsorbPoisonAC = 245,
            AbsorbNanoAC = 246,
            TemporarySkillReduction = 247,
            BirthDate = 248,
            LastSaved = 249,
            SoundVolume = 250,
            CheckPetType = 251,
            MetersWalked = 252,
            QuestLevelsSolved = 253,
            MonsterLevelsKilled = 254,
            PvPLevelsKilled = 255,
            MissionBitsA = 256,
            MissionBitsB = 257,
            AccessGrant = 258,
            DoorFlags = 259,
            ClanHierarchy = 260,
            QuestStat = 261,
            ClientActivated = 262,

            /*
Brawl1Weapon = 263,
Brawl2Weapon = 264,
DimachWeapon = 265,
MartialArtsWeapon = 266,
RiposteWeapon = 267,
            //*/
            PersonalResearchLevel = 263,
            GlobalResearchLevel = 264,
            PersonalResearchGoal = 265,
            GlobalResearchGoal = 266,
            TurnSpeed = 267,

            LiquidType = 268,
            GatherSound = 269,
            CastSound = 270,
            TravelSound = 271,
            HitSound = 272,
            SecondaryItemTemplate = 273,
            EquippedWeapons = 274,
            XPKillRange = 275,
            AddAllOffense = 276,
            AddAllDefense = 277,
            ProjectileDamageModifier = 278,
            MeleeDamageModifier = 279,
            EnergyDamageModifier = 280,
            ChemicalDamageModifier = 281,
            RadiationDamageModifier = 282,
            ItemHateValue = 283,
            CriticalBonus = 284,
            MaxDamage = 285,
            MinDamage = 286,
            AttackRange = 287,
            HateValueModifier = 288,
            TrapDifficulty = 289,
            StatOne = 290,
            NumAttackEffects = 291,
            DefaultAttackType = 292,
            ItemSkill = 293,
            AttackDelay = 294,
            ItemOpposedSkill = 295,
            ItemSIS = 296,
            InteractionRadius = 297,
            Slot = 298,
            LockDifficulty = 299,
            Members = 300,
            MinMembers = 301,
            ClanPrice = 302,
            ClanUpkeep = 303,
            ClanType = 304,
            ClanInstance = 305,
            VoteCount = 306,
            MemberType = 307,
            MemberInstance = 308,
            GlobalClanType = 309,
            GlobalClanInstance = 310,
            ColdDamageModifier = 311,
            ClanUpkeepInterval = 312,
            TimeSinceUpkeep = 313,
            ClanFinalized = 314,
            NanoDamageModifier = 315,
            FireDamageModifier = 316,
            PoisonDamageModifier = 317,
            NanoCost_Percent = 318, //%
            XPModifier_Percent = 319, //%
            BreedLimit = 320,
            GenderLimit = 321,
            LevelLimit = 322,
            PlayerKilling = 323,
            TeamAllowed = 324,
            WeaponDisallowedType = 325,
            WeaponDisallowedInstance = 326,
            Taboo = 327,
            Compulsion = 328,
            SkillDisabled = 329,
            ClanItemType = 330,
            ClanItemInstance = 331,
            DebuffFormula = 332,
            PvPRating = 333,
            SavedXP = 334,
            DoorBlockTime = 335,
            OverrideTexture = 336,
            OverrideMaterial = 337,
            DeathReason = 338,
            DamageType = 339,
            BrainType = 340,
            XPBonus_Percent = 341, //
            HealInterval = 342,
            HealDelta = 343,
            MonsterTexture = 344,
            HasAlwaysLootable = 345,
            TradeLimit = 346,
            FaceTexture = 347,
            SpecialCondition = 348,
            AutoAttackFlags = 349,
            NextXP = 350,
            TeleportPauseMilliSeconds = 351,
            SISCap = 352,
            AnimSet = 353,
            AttackType = 354,
            WornItem = 355, //CyberDeck
            NPCHash = 356,
            CollisionRadius = 357,
            OuterRadius = 358,
            ShapeShift = 359,
            Scale_Percent = 360, //%
            HitEffectType = 361,
            ResurrectDestination = 362,
            NanoInterval = 363,
            NanoDelta = 364,
            ReclaimItem = 365,
            GatherEffectType = 366,
            VisualBreed = 367,
            VisualProfession = 368,
            VisualGender = 369,
            RitualTargetInst = 370,
            SkillTimeOnSelectedTarget = 371,
            LastSaveXP = 372,
            ExtendedTime = 373,
            BurstRecharge = 374,
            FullAutoRecharge = 375,
            GatherAbstractAnim = 376,
            CastTargetAbstractAnim = 377,
            CastSelfAbstractAnim = 378,
            CriticalIncrease_Percent = 379, //%
            WeaponRange = 380,
            NanoRange = 381,
            SkillLockModifier = 382,
            NanoInterruptModifier = 383,
            EntranceStyles = 384,
            ChanceOfBreakOnNanoAttack = 385,
            ChanceOfBreakOnDebuff = 386,
            DieAnim = 387,
            TowerType = 388,
            Expansion = 389,
            LowresMesh = 390,
            CriticalResistance_Percent = 391, //%
            OldTimeExist = 392,
            ResistModifier = 393,
            ChestFlags = 394,
            PrimaryTemplateID = 395,
            NumberOfItems = 396,
            SelectedTargetType = 397,
            CorpseHash = 398,
            AmmoName = 399,
            Rotation = 400,
            CATAnim = 401,
            CATAnimFlags = 402,
            DisplayCATAnim = 403,
            DisplayCATMesh = 404,
            NanoSchool = 405,
            NanoSpeed = 406,
            NanoPoints = 407,
            TrainSkill = 408,
            TrainSkillCost = 409,
            InFight = 410,
            NextFormula = 411,
            MultipleCount = 412,
            EffectType = 413,
            ImpactEffectType = 414,
            CorpseType = 415,
            CorpseInstance = 416,
            CorpseAnimKey = 417,
            UnarmedTemplateInstance = 418,
            TracerEffectType = 419,
            AmmoType = 420,
            CharRadius = 421,
            ChanceOfUse = 422,
            CurrentState = 423,
            ArmorType = 424,
            RestModifier = 425,
            BuyModifier = 426,
            SellModifier = 427,
            CastEffectType = 428,
            NPCBrainState = 429,
            WaitState = 430,
            SelectedTarget = 431,
            ErrorCode = 432,
            OwnerInstance = 433,
            CharState = 434,
            ReadOnly = 435,
            DamageType2 = 436,
            CollideCheckInterval = 437,
            PlayfieldType = 438,
            NPCCommand = 439,
            InitiativeType = 440,
            CharTmp1 = 441,
            CharTmp2 = 442,
            CharTmp3 = 443,
            CharTmp4 = 444,
            NPCCommandArg = 445,
            NameTemplate = 446,
            DesiredTargetDistance = 447,
            VicinityRange = 448,
            NPCIsSurrendering = 449,
            StateMachine = 450,
            NPCSurrenderInstance = 451,
            NPCHasPatrolList = 452,
            NPCVicinityChars = 453,
            ProximityRangeOutdoors = 454,
            NPCFamily = 455,
            CommandRange = 456,
            NPCHatelistSize = 457,
            NPCNumPets = 458,
            ODMinSizeAdd = 459,
            EffectRed = 460,
            EffectGreen = 461,
            EffectBlue = 462,
            ODMaxSizeAdd = 463,
            DurationModifier = 464,
            NPCCryForHelpRange = 465,
            LOSHeight = 466,

//PetRequirement1 = 467,
            SLZoneProtection = 467,

            PetReq2 = 468,
            PetReq3 = 469,
            MapUpgrades = 470,
            MapFlags1 = 471,
            MapFlags2 = 472,
            FixtureFlags = 473,
            FallDamage = 474,
            MaxReflectedProjectileAC = 475,
            MaxReflectedMeleeAC = 476,
            MaxReflectedEnergyAC = 477,
            MaxReflectedChemicalAC = 478,
            MaxReflectedRadiationAC = 479,
            MaxReflectedColdAC = 480,
            MaxReflectedNanoAC = 481,
            MaxReflectedFireAC = 482,
            MaxReflectedPoisonAC = 483,
            ProximityRangeIndoors = 484,
            PetReqVal1 = 485,
            PetReqVal2 = 486,
            PetReqVal3 = 487,
            TargetFacing = 488,
            Backstab = 489,
            OriginatorType = 490,
            QuestInstance = 491,
            QuestIndex1 = 492,
            QuestIndex2 = 493,
            QuestIndex3 = 494,
            QuestIndex4 = 495,
            QuestIndex5 = 496,
            QTDungeonInstance = 497,
            QTNumMonsters = 498,
            QTKilledMonsters = 499,
            AnimPos = 500,
            AnimPlay = 501,
            AnimSpeed = 502,
            QTKillNumMonsterID1 = 503,
            QTKillNumMonsterCount1 = 504,
            QTKillNumMonsterID2 = 505,
            QTKillNumMonsterCount2 = 506,
            QTKillNumMonsterID3 = 507,
            QTKillNumMonsterCount3 = 508,
            QuestIndex0 = 509,
            QuestTimeout = 510,
            TowerNPCHash = 511,
            PetType = 512,
            OnTowerCreation = 513,
            OwnedTowers = 514,
            TowerInstance = 515,
            AttackShield = 516,
            SpecialAttackShield = 517,
            NPCVicinityPlayers = 518,
            NPCUseFightModeRegenRate = 519,
            RandomNumberRoll = 520,
            SocialStatus = 521,
            LastRnd = 522,
            AttackDelayCap = 523,
            RechargeDelayCap = 524,
            RemainingHealth_Percent = 525, //%
            RemainingNano_Percent = 526, //%
            TargetDistance = 527,
            TeamLevel = 528,
            NumberOnHateList = 529,
            ConditionState = 530,
            ExpansionPlayfield = 531,
            ShadowBreed = 532,
            NPCFovStatus = 533,
            DudChance = 534,
            HealModifier_Percent = 535, //%
            NanoDamage_Percent = 536, //%
            NanoVulnerability = 537,
            AMSCap = 538,
            ProcInitiative1 = 539,
            ProcInitiative2 = 540,
            ProcInitiative3 = 541,
            ProcInitiative4 = 542,
            FactionModifier = 543,
            StackingLine2 = 546,
            StackingLine3 = 547,
            StackingLine4 = 548,
            StackingLine5 = 549,
            StackingLine6 = 550,
            StackingOrder = 551,
            ProcNano1 = 552,
            ProcNano2 = 553,
            ProcNano3 = 554,
            ProcNano4 = 555,
            ProcChance1 = 556,
            ProcChance2 = 557,
            ProcChance3 = 558,
            ProcChance4 = 559,
            OTArmedForces = 560,
            ClanSentinels = 561,
            OTMed = 562,
            ClanGaia = 563,
            OTTrans = 564,
            ClanVanguards = 565,
            GaurdianOfShadows = 566,
            OTFollowers = 567,
            OTOperator = 568,
            OTUnredeemed = 569,
            ClanDevoted = 570,
            ClanConserver = 571,
            ClanRedeemed = 572,
            SK = 573,
            LastSK = 574,
            NextSK = 575,
            PlayerOptions = 576,
            LastPerkResetTime = 577,
            CurrentTime = 578,
            ShadowBreedTemplate = 579,
            NPCVicinityFamily = 580,
            NPCScriptAMSScale = 581,
            ApartmentsAllowed = 582,
            ApartmentsOwned = 583,
            ApartmentAccessCard = 584,
            MapFlags3 = 585,
            MapFlags4 = 586,
            NumberOfTeamMembers = 587,
            ActionCategory = 588,
            CurrentPlayfield = 589,
            DistrictNano = 590,
            DistrictNanoInterval = 591,
            UnsavedXP = 592,
            RegainXP_Percent = 593, //%

            TempSaveTeamID = 594,
            TempSavePlayfield = 595,
            TempSaveX = 596,
            TempSaveY = 597,
            ExtendedFlags = 598,
            ShopPrice = 599,
            NewbieHP = 600,
            HPLevelUp = 601,
            HPPerSkill = 602,
            NewbieNP = 603,
            NPLevelUp = 604,
            NPPerSkill = 605,
            MaxShopItems = 606,
            PlayerID = 607,
            ShopRent = 608,
            SynergyHash = 609,
            ShopFlags = 610,
            ShopLastUsed = 611,
            ShopType = 612,
            LockDownTime = 613,
            LeaderLockDownTime = 614,
            InvadersKilled = 615,
            KilledByInvaders = 616,
            HouseTemplate = 620,
            FireDamage_Percent = 621, //%
            ColdDamage_Percent = 622, //%
            MeleeDamage_Percent = 623, //%
            ProjectileDamage_Percent = 624, //%
            PoisonDamage_Percent = 625, //%
            RadiationDamage_Percent = 626, //%
            EnergyDamage_Percent = 627, //%
            ChemicalDamage_Percent = 628, //%
            TotalDamage = 629,
            TrackProjectileDamage = 630,
            TrackMeleeDamage = 631,
            TrackEnergyDamage = 632,
            TrackChemicalDamage = 633,
            TrackRadiationDamage = 634,
            TrackColdDamage = 635,
            TrackPoisonDamage = 636,
            TrackFireDamage = 637,
            NPCSpellArg1 = 638,
            NPCSpellRet1 = 639,
            CityInstance = 640,
            DistanceToSpawnpoint = 641,
            CityTerminalRechargePercent = 642,
            AdvantageHash1 = 651,
            AdvantageHash2 = 652,
            AdvantageHash3 = 653,
            AdvantageHash4 = 654,
            AdvantageHash5 = 655,
            ShopIndex = 656,
            ShopID = 657,
            IsVehicle = 658,
            DamageToNano_Percent = 659, //% ***

            AccountFlags = 660,
            DamageToNano_Percent2 = 661, //% *** ???
            MechData = 662,
            PointValue = 663,
            VehicleAC = 664,
            VehicleDamage = 665,
            VehicleHealth = 666,
            VehicleSpeed = 667,
            BattlestationFaction = 668,
            VP = 669,
            BattlestationRep = 670,
            PetState = 671,
            PaidPoints = 672,

            ItemSeed = 700,
            ItemLevel = 701,
            ItemTemplateID = 702,
            ItemTemplateID2 = 703,
            ItemCategoryID = 704,
            HasKnubotData = 768,
            QuestBoothDifficulty = 800,
            QuestASMinimumRange = 801,
            QuestASMaximumRange = 802,
            VisualLODLevel = 888,
            TargetDistanceChange = 889,
            TideRequiredDynelID = 900,
            StreamCheckMagic = 999,

            RecordType = 1001, //***
            RecordInstance = 1002, //***

            NoStatValue = 1234567890, //<-- this is default

        /*
            //[1] ; Alternates
            ClanTokens = 62,
            OmniTokens = 75,
        //*/
        }

        private const RegexOptions Options =
            RegexOptions.Compiled
            | RegexOptions.CultureInvariant
            | RegexOptions.ExplicitCapture
            //| RegexOptions.IgnoreCase <-- bug warning! DO NOT USE, apply to specific pattern
            | RegexOptions.IgnorePatternWhitespace
            | RegexOptions.Multiline
            | RegexOptions.Singleline;

        //private static readonly Regex IsNumbericRegex = new Regex(@"^\d+$", Options);
        //private static readonly Regex IsHashCodeRegex = new Regex(@"^(?<string>[A-Z0-9]){4}", Options);
        //private static readonly Regex IsStringRegex = new Regex(@"^(?<string>[\t\r\n\x20-\x7f]{3,})\0", Options); // this will fail to detect null/empty strings

        private BotShell _bot;

        public BbAoDb()
        {
            Name = "BbAoDb :: Core";
            InternalName = "BbAodbCore";
            Version = 100;
            Author = "Bitbucket";
            DefaultState = PluginState.Installed;
            Description = "Bitbucket's AO Database";
        }


        public override void OnLoad(BotShell bot)
        {
            _bot = bot;
        }


        /// <summary>
        ///     AO Versioning
        /// </summary>
        public sealed class AoVersion : IComparable<AoVersion>, IEquatable<AoVersion>
        {
            private const string Pattern = @"^\s*(?<major>\d+)"
                                           + @"\.(?<minor>\d+)"
                                           + @"(?:\.(?<build>\d+))?"
                                           + @"(?:\.(?<revision>\d+))?"
                                           + @"(?:_EP(?<expansion>\d+))?\s*$";


            private const Int32 Mask = 0x3F; // 6 bit mask

            private readonly Int32 _hashCode;
            private readonly Regex _parser = new Regex(Pattern, Options | RegexOptions.IgnoreCase);

            /// <summary>
            ///     Initializes a new instance of the <see cref="AoVersion" /> class.
            /// </summary>
            /// <param name="major">The major.</param>
            /// <param name="minor">The minor.</param>
            /// <param name="build">The build.</param>
            /// <param name="revision">The revision.</param>
            /// <param name="expansion">The expansion.</param>
            public AoVersion(byte major, byte minor, byte? build = null, byte? revision = null, byte? expansion = null)
            {
                Major = major;
                Minor = minor;
                Build = build;
                Revision = revision;
                Expansion = expansion;
                _hashCode = CreateHashCode();
            }

            /// <summary>
            ///     Initializes a new instance of the <see cref="AoVersion" /> class.
            /// </summary>
            /// <param name="version">The version.</param>
            /// <exception cref="System.ArgumentException">Version string is not in the correct format</exception>
            public AoVersion(string version)
            {
                var match = _parser.Match(version);
                if (!match.Success)
                    throw new ArgumentException("Version string is not in the correct format");

                var i = 0;
                Major = Byte.Parse("0" + match.Groups[++i].Value);
                Minor = Byte.Parse("0" + match.Groups[++i].Value);
                if (match.Groups[++i].Success)
                    Build = Byte.Parse("0" + match.Groups[i].Value);

                if (match.Groups[++i].Success)
                    Revision = Byte.Parse("0" + match.Groups[i].Value);

                if (match.Groups[++i].Success)
                    Expansion = Byte.Parse("0" + match.Groups[i].Value);

                _hashCode = CreateHashCode();
            }

            /// <summary>
            ///     Initializes a new instance of the <see cref="AoVersion" /> class.
            /// </summary>
            /// <param name="hashCode">The hash code.</param>
            public AoVersion(Int32 hashCode)
            {
                var hasExpac = (hashCode & 0x4000000) != 0;
                var hasRevis = (hashCode & 0x2000000) != 0;
                var hasBuild = (hashCode & 0x1000000) != 0;

                if (hasRevis)
                    Revision = (byte?) (hashCode & Mask);
                hashCode >>= 6;

                if (hasBuild)
                    Build = (byte?) (hashCode & Mask);
                hashCode >>= 6;

                Minor = (byte) (hashCode & Mask);
                hashCode >>= 6;

                Major = (byte) (hashCode & Mask);
                hashCode >>= 6;

                if (hasExpac)
                    Expansion = (byte?) (hashCode & 0xF);
                _hashCode = CreateHashCode();
            }

            public byte Major { get; private set; }
            public byte Minor { get; private set; }
            public byte? Build { get; private set; }
            public byte? Revision { get; private set; }
            public byte? Expansion { get; private set; }


            /// <summary>
            ///     Compares the current instance with another object of the same type and returns an integer that indicates whether
            ///     the current instance precedes, follows, or occurs in the same position in the sort order as the other object.
            /// </summary>
            /// <param name="other">An object to compare with this instance.</param>
            /// <returns>
            ///     A value that indicates the relative order of the objects being compared. The return value has these meanings: Value
            ///     Meaning Less than zero This instance precedes <paramref name="other" /> in the sort order.  Zero This instance
            ///     occurs in the same position in the sort order as <paramref name="other" />. Greater than zero This instance follows
            ///     <paramref name="other" /> in the sort order.
            /// </returns>
            public int CompareTo(AoVersion other)
            {
                const Int32 mask = -1 >> 3; // mask off the bitflags before compare

                // use a subtractive comparison
                return (_hashCode & mask)
                       - (other._hashCode & mask);
            }

            /// <summary>
            ///     Indicates whether the current object is equal to another object of the same type.
            /// </summary>
            /// <param name="other">An object to compare with this object.</param>
            /// <returns>
            ///     true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
            /// </returns>
            public bool Equals(AoVersion other)
            {
                return other != null
                       && _hashCode.Equals(other._hashCode);
            }

            /// <summary>
            ///     Creates the hash code.
            /// </summary>
            /// <returns></returns>
            private Int32 CreateHashCode()
            {
                /* 31                                   7       0
                 *  0000 0000 : 0000 0000 : 0000 0000 : 0000 0000
                 *  |||| ||||   |||| ||||   |||| ||||   |||| ||||
                 *  |||| ||||   |||| ||||   |||| ||||   ||++-++++--> Revision (6 bits)
                 *  |||| ||||   |||| ||||   |||| ++++---++--> Build (6 bits)
                 *  |||| ||||   |||| ||++---++++--> Minor (6 bits)
                 *  |||| ||||   ++++-++--> Major (6 bits)
                 *  |||| ++++--> Expansion (4 bits)
                 *  |||+--> Null Flag: Build
                 *  ||+--> Null Flag: Revision
                 *  |+--> Null Flag: Expansion
                 *  +--> Unused (sign bit)
                 *  
                 * Bitflags and expansion are given the highest order because they will rarely ever change.
                 * Everything else, goes down the priority chain.
                 */
                return (Expansion.Equals(null) ? 0 : 0x40000000)
                       | (Revision.Equals(null) ? 0 : 0x20000000)
                       | (Build.Equals(null) ? 0 : 0x10000000)
                       | (((Revision ?? 0) & Mask) << 0)
                       | (((Build ?? 0) & Mask) << 6)
                       | ((Minor & Mask) << 12)
                       | ((Major & Mask) << 18)
                       | (((Expansion ?? 0) & 0xF) << 24);
            }

            /// <summary>
            ///     Returns a hash code for this instance.
            /// </summary>
            /// <returns>
            ///     A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
            /// </returns>
            public override int GetHashCode()
            {
                return _hashCode;
            }

            /// <summary>
            ///     Returns a <see cref="System.String" /> that represents this instance.
            /// </summary>
            /// <returns>
            ///     A <see cref="System.String" /> that represents this instance.
            /// </returns>
            public override string ToString()
            {
                var result = String.Format("{0}.{1}", Major, Minor);

                if (!Build.Equals(null))
                    result += String.Format(".{0}", Build);
                if (!Revision.Equals(null))
                    result += String.Format(".{0}", Revision);
                if (!Expansion.Equals(null))
                    result += String.Format("_EP{0}", Expansion);

                return result;
            }
        }


        /// <summary>
        ///     Class for binary input
        /// </summary>
        public sealed class ByteStream
        {
            private readonly Dictionary<TypeCode, Int32> _sizes;

            private int _ptr;

            /// <summary>
            ///     Initializes a new instance of the <see cref="ByteStream" /> class.
            /// </summary>
            /// <param name="buffer">The buffer.</param>
            public ByteStream(byte[] buffer)
            {
                Buffer = buffer;

                // do not use sizeof() it can cause bugs.
                _sizes = new Dictionary<TypeCode, int>
                         {
                             {TypeCode.Boolean, 1},
                             {TypeCode.Byte, 1},
                             {TypeCode.SByte, 1},
                             {TypeCode.Char, 2},
                             {TypeCode.Int16, 2},
                             {TypeCode.UInt16, 2},
                             {TypeCode.Int32, 4},
                             {TypeCode.UInt32, 4},
                             {TypeCode.Single, 4},
                             {TypeCode.Double, 8},
                             {TypeCode.Int64, 8},
                             {TypeCode.UInt64, 8}
                         };
            }


            /// <summary>
            ///     Gets the buffer.
            /// </summary>
            /// <value>
            ///     The buffer.
            /// </value>
            public Byte[] Buffer { get; private set; }


            /// <summary>
            ///     Gets the length of the buffer.
            /// </summary>
            /// <value>
            ///     The length.
            /// </value>
            public int Length
            {
                get { return Buffer.Length; }
            }


            /// <summary>
            ///     Gets or sets the position.
            /// </summary>
            /// <value>
            ///     The position.
            /// </value>
            /// <exception cref="System.ArgumentOutOfRangeException">
            ///     value;value cannot be less than zero.
            ///     or
            ///     value
            /// </exception>
            public int Position
            {
                get { return _ptr; }

                set
                {
                    if (value < 0)
                        throw new ArgumentOutOfRangeException("value", value, "value cannot be less than zero.");
                    if (value > Buffer.Length + 1)
                        throw new ArgumentOutOfRangeException("value", value, string.Format("value cannot exceed buffer length. Buffer size: {0}", Buffer.Length));
                    _ptr = value;
                }
            }


            /// <summary>
            ///     Gets a value indicating whether [end of stream].
            /// </summary>
            /// <value>
            ///     <c>true</c> if [end of stream]; otherwise, <c>false</c>.
            /// </value>
            public bool EndOfStream
            {
                get { return _ptr > Buffer.Length; }
            }


            /// <summary>
            ///     Reads the specified bit converter function.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="bitConverterFunction">The bit converter function.</param>
            /// <returns></returns>
            /// <exception cref="System.IO.EndOfStreamException"></exception>
            private T Read<T>(BitConverterFunction<T> bitConverterFunction)
            {
                var typeCode = Type.GetTypeCode(typeof (T));
                var size = _sizes[typeCode];

                if (_ptr > Buffer.Length - size)
                    throw new EndOfStreamException();

                var val = bitConverterFunction(Buffer, _ptr);
                _ptr += size;
                return val;
            }


            /// <summary>
            ///     Reads the byte.
            /// </summary>
            /// <returns></returns>
            /// <exception cref="System.IO.EndOfStreamException"></exception>
            public Byte ReadByte()
            {
                if (_ptr > Buffer.Length - 1)
                    throw new EndOfStreamException();
                return Buffer[_ptr++];
            }


            /// <summary>
            ///     Reads the bytes.
            /// </summary>
            /// <param name="count">The count.</param>
            /// <returns></returns>
            /// <exception cref="System.ArgumentOutOfRangeException">count;count cannot be less than zero.</exception>
            /// <exception cref="System.IO.EndOfStreamException"></exception>
            public Byte[] ReadBytes(int count)
            {
                if (count < 0)
                    throw new ArgumentOutOfRangeException("count", count, "count cannot be less than zero.");
                if (_ptr > Buffer.Length - count)
                    throw new EndOfStreamException();
                var val = new byte[count];
                Array.Copy(Buffer, _ptr, val, 0, count);
                _ptr += count;
                return val;
            }


            /// <summary>
            ///     Reads the Int16.
            /// </summary>
            /// <returns></returns>
            /// <exception cref="System.IO.EndOfStreamException"></exception>
            public Int16 ReadInt16()
            {
                return Read(BitConverter.ToInt16);
            }


            /// <summary>
            ///     Reads the UInt16.
            /// </summary>
            /// <returns></returns>
            /// <exception cref="System.IO.EndOfStreamException"></exception>
            public UInt16 ReadUInt16()
            {
                return Read(BitConverter.ToUInt16);
            }


            /// <summary>
            ///     Reads the Int32.
            /// </summary>
            /// <returns></returns>
            /// <exception cref="System.IO.EndOfStreamException"></exception>
            public Int32 ReadInt32()
            {
                return Read(BitConverter.ToInt32);
            }


            /// <summary>
            ///     Reads the UInt32.
            /// </summary>
            /// <returns></returns>
            /// <exception cref="System.IO.EndOfStreamException"></exception>
            public UInt32 ReadUInt32()
            {
                return Read(BitConverter.ToUInt32);
            }


            /// <summary>
            ///     Reads the Int64.
            /// </summary>
            /// <returns></returns>
            /// <exception cref="System.IO.EndOfStreamException"></exception>
            public Int64 ReadInt64()
            {
                return Read(BitConverter.ToInt64);
            }


            /// <summary>
            ///     Reads the UInt64.
            /// </summary>
            /// <returns></returns>
            /// <exception cref="System.IO.EndOfStreamException"></exception>
            public UInt64 ReadUInt64()
            {
                return Read(BitConverter.ToUInt64);
            }


            /// <summary>
            ///     Reads the single.
            /// </summary>
            /// <returns></returns>
            public Single ReadSingle()
            {
                return Read(BitConverter.ToSingle);
            }


            /// <summary>
            ///     Reads the double.
            /// </summary>
            /// <returns></returns>
            public Double ReadDouble()
            {
                return Read(BitConverter.ToDouble);
            }


            /// <summary>
            ///     Reads the UTF-8 string.
            /// </summary>
            /// <param name="length">The length.</param>
            /// <returns></returns>
            /// <exception cref="System.ArgumentOutOfRangeException">length;length cannot be less than zero.</exception>
            /// <exception cref="System.IO.EndOfStreamException"></exception>
            public String ReadStringUtf8(int length)
            {
                if (length < 0)
                    throw new ArgumentOutOfRangeException("length", length, "length cannot be less than zero.");
                if (_ptr > Buffer.Length - length)
                    throw new EndOfStreamException();
                var val = Encoding.UTF8.GetString(Buffer, _ptr, length);
                _ptr += length;
                return val;
            }


            /// <summary>
            ///     Reads the UTF-8 Zero-Terminated String.
            /// </summary>
            /// <returns></returns>
            /// <exception cref="System.IO.EndOfStreamException"></exception>
            public String ReadZStringUtf8()
            {
                var sb = new StringBuilder();

                while (Buffer[_ptr] != 0)
                {
                    if (_ptr > Buffer.Length - 1)
                        throw new EndOfStreamException();
                    sb.Append(Buffer[_ptr++]);
                }

                _ptr++; // skip over the null byte
                return sb.ToString();
            }

            private delegate T BitConverterFunction<out T>(Byte[] buffer, Int32 index);
        }


        /// <summary>
        ///     RDB Reader
        /// </summary>
        public sealed class RdbReader
        {
            /// <summary>
            ///     Known RDB Record Types
            /// </summary>
            public enum ERecordType : uint
            {
                Items = 0xF4254,
                Nanoprograms = 0xFDE85,
            }

            private static readonly Regex FileFilter = new Regex(@"ResourceDatabase\.dat(?:\.(?<num>\d+))?$", Options);

            private readonly Dictionary<uint, BinaryReader> _datFiles;
            private readonly DIC_T _indexes;

            /// <summary>
            ///     Initializes a new instance of the <see cref="RdbReader" /> class.
            /// </summary>
            /// <param name="aoInstallPath">The ao install path.</param>
            public RdbReader(string aoInstallPath)
            {
                AoInstallPath = aoInstallPath;

                // open data files
                var path = CombinePath(aoInstallPath, @"cd_image\data\db");
                _datFiles = new Dictionary<uint, BinaryReader>();
                foreach (var file in Directory.EnumerateFiles(path))
                {
                    var match = FileFilter.Match(file);
                    if (!match.Success)
                        continue;

                    var key = UInt32.Parse("0" + match.Groups[1].Value);
                    //if (!_datFiles.ContainsKey(key))
                    _datFiles.Add(key, new BinaryReader(new FileStream(file, FileMode.Open, FileAccess.Read)));
                }

                // open the index and get some header information
                path = CombinePath(path, "ResourceDatabase.idx");
                var idx = new BinaryReader(new FileStream(path, FileMode.Open, FileAccess.Read));
                idx.BaseStream.Seek(0x0c, SeekOrigin.Begin);
                BlockOffset = idx.ReadUInt32();

                idx.BaseStream.Seek(0xb8, SeekOrigin.Begin);
                DataFileSize = idx.ReadUInt32();

                idx.BaseStream.Seek(0x48, SeekOrigin.Begin);
                var nextBlock = idx.ReadUInt32();

                // get the RDB version
                path = CombinePath(aoInstallPath, "version.id");
                var verstr = File.ReadAllText(path);
                var aover = new AoVersion(verstr);


                // create an index reader based on the version
                byte? v2 = 2;
                IdxBase indexer;
                if (aover.Expansion.Equals(v2))
                    indexer = new IdxNewEngine(DataFileSize, BlockOffset);
                else
                    indexer = new IdxSplit(DataFileSize, BlockOffset);


                // start reading indicies
                _indexes = new DIC_T();
                while (nextBlock > 0)
                {
                    idx.BaseStream.Seek(nextBlock, SeekOrigin.Begin);
                    nextBlock = idx.ReadUInt32();
                    idx.ReadBytes(4); // previousBlock not required
                    var entries = idx.ReadUInt16();
                    idx.ReadBytes(0x12); // unknown

                    while (entries-- > 0) // a more elegant for loop
                    {
                        indexer.ReadNext(idx);

                        var kvp = new KeyValuePair<uint, long>(indexer.Volume, indexer.RecordPosition);

                        if (!_indexes.ContainsKey(indexer.RecordType))
                            _indexes.Add(indexer.RecordType, new Dictionary<uint, KeyValuePair<uint, long>>());

                        if (!_indexes[indexer.RecordType].ContainsKey(indexer.RecordInstance))
                            _indexes[indexer.RecordType].Add(indexer.RecordInstance, kvp);
                        else
                            _indexes[indexer.RecordType][indexer.RecordInstance] = kvp;
                    } // while
                } // while
            }

            /// <summary>
            ///     Gets the size of the data file. (for debug use)
            /// </summary>
            /// <value>
            ///     The size of the data file.
            /// </value>
            public UInt32 DataFileSize { get; private set; }

            /// <summary>
            ///     Gets the block offset. (for debug use)
            /// </summary>
            /// <value>
            ///     The block offset.
            /// </value>
            public UInt32 BlockOffset { get; private set; }

            // constructor method

            /// <summary>
            ///     Gets the ao install path.
            /// </summary>
            /// <value>
            ///     The ao install path.
            /// </value>
            public string AoInstallPath { get; private set; }

            /// <summary>
            ///     Gets the data files.
            /// </summary>
            /// <returns></returns>
            public Dictionary<uint, BinaryReader> GetDataFiles()
            {
                return _datFiles;
            }

            /// <summary>
            ///     Gets the indicies.
            /// </summary>
            /// <returns></returns>
            public DIC_T GetIndicies()
            {
                return _indexes;
            }

            /// <summary>
            ///     Gets the record types.
            /// </summary>
            /// <returns></returns>
            public UInt32[] GetRecordTypes()
            {
                return _indexes.Keys.ToArray();
            }

            /// <summary>
            ///     Gets the record instances.
            /// </summary>
            /// <param name="recordType">Type of the record.</param>
            /// <returns></returns>
            public UInt32[] GetRecordInstances(UInt32 recordType)
            {
                return _indexes.ContainsKey(recordType)
                           ? _indexes[recordType].Keys.ToArray()
                           : null;
            }

            /// <summary>
            ///     Determines whether the specified record type exists.
            /// </summary>
            /// <param name="recordType">Type of the record.</param>
            /// <returns></returns>
            public bool HasRecordType(UInt32 recordType)
            {
                return _indexes.ContainsKey(recordType);
            }

            /// <summary>
            ///     Determines whether the specified record type and record instance exists.
            /// </summary>
            /// <param name="recordType">Type of the record.</param>
            /// <param name="recordInstance">The record instance.</param>
            /// <returns></returns>
            public bool HasRecordInstance(UInt32 recordType, UInt32 recordInstance)
            {
                return _indexes.ContainsKey(recordType)
                       && _indexes[recordType].ContainsKey(recordInstance);
            }

            /// <summary>
            ///     Gets the record.
            /// </summary>
            /// <param name="recordType">Type of the record.</param>
            /// <param name="recordInstance">The record instance.</param>
            /// <returns></returns>
            /// <exception cref="System.Exception">
            ///     Record type not does not exist
            ///     or
            ///     Record instance does not exist
            /// </exception>
            public byte[] GetRecord(UInt32 recordType, UInt32 recordInstance)
            {
                if (!_indexes.ContainsKey(recordType))
                    throw new Exception("Record type not does not exist");

                if (!_indexes[recordType].ContainsKey(recordInstance))
                    throw new Exception("Record instance does not exist");

                var kvp = _indexes[recordType][recordInstance];
                var df = _datFiles[kvp.Key];

                // read the record
                df.BaseStream.Seek(kvp.Value, SeekOrigin.Begin);
                var header = new RecordHeader(df);

                if (!header.IsValid)
                    throw new InvalidDataException("Failed to read RDB record");

                return df.ReadBytes((int) header.RecordLength);
            }

            // private utility function
            private static string CombinePath(params string[] parts)
            {
                const string sep = @"\"; // Path.PathSeparator.ToString(CultureInfo.InvariantCulture);
                const string dsep = @"\\";
                return String.Join(sep, parts).Replace(dsep, sep);
            }


            /// <summary>
            ///     Index Reader Base
            /// </summary>
            private abstract class IdxBase
            {
                protected readonly UInt32 BlockOffset;
                protected readonly UInt32 DataFileSize;

                /// <summary>
                ///     Initializes a new instance of the <see cref="IdxBase" /> class.
                /// </summary>
                /// <param name="dataFileSize">Size of the data file.</param>
                /// <param name="blockOffset">The block offset.</param>
                protected IdxBase(UInt32 dataFileSize, UInt32 blockOffset)
                {
                    DataFileSize = dataFileSize;
                    BlockOffset = blockOffset;
                }

                public UInt32 Volume { get; protected set; }
                public UInt32 RecordType { get; protected set; }
                public UInt32 RecordInstance { get; protected set; }
                public UInt32 RecordPosition { get; protected set; }

                /// <summary>
                ///     Swaps the endian.
                /// </summary>
                /// <param name="value">The value.</param>
                /// <returns></returns>
                protected static UInt32 SwapEndian(UInt32 value)
                {
                    var bytes = BitConverter.GetBytes(value);
                    Array.Reverse(bytes);
                    return BitConverter.ToUInt32(bytes, 0);
                }

                /// <summary>
                ///     Reads the next index.
                /// </summary>
                /// <param name="br">The br.</param>
                public abstract void ReadNext(BinaryReader br);
            }


            /// <summary>
            ///     With the release of the new engine, the database grew again and was split into many volumes.
            ///     and with the split, the index was changed so some of Big Mac's Fancy Mathematics is required.
            /// </summary>
            private sealed class IdxNewEngine : IdxBase
            {
                public IdxNewEngine(uint dataFileSize, uint blockOffset) : base(dataFileSize, blockOffset)
                {
                }

                /// <summary>
                ///     Reads the next index.
                /// </summary>
                /// <param name="br">The br.</param>
                public override void ReadNext(BinaryReader br)
                {
                    Volume = br.ReadUInt32();
                    RecordPosition = br.ReadUInt32();
                    RecordType = SwapEndian(br.ReadUInt32());
                    RecordInstance = SwapEndian(br.ReadUInt32());

                    // Big Mac's Fancy Mathematics
                    var delta = RecordPosition / DataFileSize;
                    Volume *= 4;
                    Volume += delta;

                    RecordPosition += (BlockOffset * Volume);
                    RecordPosition -= (DataFileSize * delta);
                }
            }

            /// <summary>
            ///     after circa 18.2 the database grew too big to keep in a single file so it was split;
            ///     this is still in use for the old-engine clients.
            /// </summary>
            private sealed class IdxSplit : IdxBase
            {
                public IdxSplit(uint dataFileSize, uint blockOffset) : base(dataFileSize, blockOffset)
                {
                }

                /// <summary>
                ///     Reads the next index.
                /// </summary>
                /// <param name="br">The br.</param>
                public override void ReadNext(BinaryReader br)
                {
                    Volume = br.ReadUInt32();
                    RecordPosition = br.ReadUInt32();
                    RecordType = SwapEndian(br.ReadUInt32());
                    RecordInstance = SwapEndian(br.ReadUInt32());

                    // apply corrections below
                    var delta = RecordPosition / DataFileSize;
                    Volume += delta;
                    RecordPosition += (BlockOffset * delta);
                    RecordPosition -= (DataFileSize * delta);
                }
            }


            /// <summary>
            ///     RDB Record Header
            /// </summary>
            public sealed class RecordHeader
            {
                /// <summary>
                ///     The size of the RDB Record Header
                /// </summary>
                public const Int64 SizeOf = 0x22;

                /// <summary>
                ///     Initializes a new instance of the <see cref="RecordHeader" /> class.
                /// </summary>
                /// <param name="br">The br.</param>
                public RecordHeader(BinaryReader br)
                {
                    var flags = br.ReadUInt16();
                    br.ReadBytes(8); // unknown
                    RecordType = br.ReadUInt32();
                    RecordInstance = br.ReadUInt32();
                    RecordLength = br.ReadUInt32() - 0x0c; // must subtract 3 ints from header

                    var rt = br.ReadUInt32();
                    var ri = br.ReadUInt32();
                    br.ReadBytes(4); // unknown

                    const UInt16 validFlag = 0xFAFA;
                    if (!flags.Equals(validFlag))
                        return;
                    if (!RecordType.Equals(rt))
                        return;
                    if (!RecordInstance.Equals(ri))
                        return;

                    IsValid = true;
                }

                /// <summary>
                ///     Gets the type of the record.
                /// </summary>
                /// <value>
                ///     The type of the record.
                /// </value>
                public UInt32 RecordType { get; private set; }

                /// <summary>
                ///     Gets the record instance.
                /// </summary>
                /// <value>
                ///     The record instance.
                /// </value>
                public UInt32 RecordInstance { get; private set; }

                /// <summary>
                ///     Gets the length of the record.
                /// </summary>
                /// <value>
                ///     The length of the record.
                /// </value>
                public UInt32 RecordLength { get; private set; }

                /// <summary>
                ///     Gets a value indicating whether this instance is valid.
                /// </summary>
                /// <value>
                ///     <c>true</c> if this instance is valid; otherwise, <c>false</c>.
                /// </value>
                public bool IsValid { get; private set; }
            }
        }
    }
}
